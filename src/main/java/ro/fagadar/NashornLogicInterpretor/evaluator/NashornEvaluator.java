package ro.fagadar.NashornLogicInterpretor.evaluator;

import java.lang.reflect.Field;
import java.sql.Date;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import ro.fagadar.NashornLogicInterpretor.dto.EvaluatorResult;

@Slf4j
@Component
public class NashornEvaluator {

	@Lazy
	@Autowired
	ScriptEngineEvaluator scriptEngineEvaluator;

	public EvaluatorResult evaluateExclusionRule(String exclusionRule, Map<String, Object> variables) {

		EvaluatorResult evaluatorResult = new EvaluatorResult();

		evaluatorResult.setEvaluationResult(false);
		evaluatorResult.setEvaluationError(false);
		evaluatorResult.setEvaluationErrorMessage("");

		if (scriptEngineEvaluator.getScriptEngine() == null) {
			log.debug("Script Engine is not enabled");
			evaluatorResult.setEvaluationErrorMessage("Script Engine is not enabled");
			return evaluatorResult;
		}

		exclusionRule = computeRule(exclusionRule, variables);
		try {
			log.debug("Evaluate : " + exclusionRule);

			if (scriptEngineEvaluator.getScriptEngine() == null) {
				log.debug("Script engine is null");
				evaluatorResult.setEvaluationErrorMessage("Script engine is null");
				return evaluatorResult;
			}
			Boolean result = (Boolean) scriptEngineEvaluator.getScriptEngine().eval(exclusionRule);
			log.debug("Expression evaluated to : " + result);
			evaluatorResult.setEvaluationResult(result);
			return evaluatorResult;
		} catch (ScriptException t) {
			log.error("Failed to evaluate exclusion rule", t);
			evaluatorResult.setEvaluationError(true);
			evaluatorResult.setEvaluationErrorMessage(t.getMessage());
		}
		return evaluatorResult;
	}

	public String computeRule(String exclusionRule, Map<String, Object> variables) {
		for (Map.Entry<String, Object> entry : variables.entrySet()) {
			String key = "%" + entry.getKey() + "%";
			if (exclusionRule.contains(key)) {
				Object o = entry.getValue();
				if (o instanceof Integer || o instanceof Double || o instanceof Long || o instanceof Boolean) {
					exclusionRule = exclusionRule.replace(key, o.toString());
				} else {
					exclusionRule = exclusionRule.replace(key, "\"" + o.toString() + "\"");
				}
			}
		}
		// regexp (do not process)
		if (exclusionRule.contains("(r)")) {
			exclusionRule = exclusionRule.replace("(r)", "");
		} else {
			// logical ones
			exclusionRule = exclusionRule.replaceAll("(?i) OR ", " || ");
			exclusionRule = exclusionRule.replaceAll("(?i) AND ", " && ");
			exclusionRule = exclusionRule.replace(" = ", " == ");
			exclusionRule = exclusionRule.replace("?", ".");
			exclusionRule = exclusionRule.replace("*", ".*");
		}
		return exclusionRule;
	}

	public Map<String, Object> getAllFieldsAndValues(String baseName, Object myObject) {
		Map<String, Object> map = new HashMap<>();
		getAllFieldsAndValues(baseName, myObject, map);
		return map;
	}

	public void getAllFieldsAndValues(String baseName, Object myObject, Map<String, Object> variables) {
		// null should be null
		if (myObject == null) {
			variables.put(baseName, null);
			return;
		}

		Class<? extends Object> classOfMyObject = myObject.getClass();
		// base class
		if (classOfMyObject.getCanonicalName().startsWith("java.lang")) {
			variables.put(baseName, myObject);
			return;
		}
		// failsafe
		if (classOfMyObject.isPrimitive() || classOfMyObject.isEnum() || myObject instanceof String
				|| myObject instanceof Integer || myObject instanceof Double || myObject instanceof Long
				|| myObject instanceof Boolean || myObject instanceof Date || myObject instanceof Instant
				|| myObject instanceof Collection) {
			variables.put(baseName, myObject);
			return;
		}

		// recursion of fields
		Field[] fields = classOfMyObject.getDeclaredFields();
		Arrays.stream(fields).forEach(field -> {
			try {
				field.setAccessible(true);
				getAllFieldsAndValues(baseName + "." + field.getName(), field.get(myObject), variables);
			} catch (Exception t) {
				log.error("Cannot retrieve field value from Object", t);
				log.error("field name: {}", field.getName());
				log.error("field class: {}", field.getClass().toString());
			}
		});

	}

	public Map<String, String> getAllFieldsAndTypes(String baseName, Object myObject) {
		Map<String, String> map = new HashMap<>();
		getAllFieldsAndTypes(baseName, myObject, map);
		return map;
	}

	public void getAllFieldsAndTypes(String baseName, Object myObject, Map<String, String> variables) {
		// null should be null
		if (myObject == null) {
			variables.put(baseName, null);
			return;
		}

		Class<? extends Object> classOfMyObject = myObject.getClass();
		// base class
		if (classOfMyObject.getCanonicalName().startsWith("java.lang")) {
			variables.put(baseName, classOfMyObject.getName());
			return;
		}
		if (isPrimitive(myObject, classOfMyObject)) {
			variables.put(baseName, classOfMyObject.getName());
			return;
		}

		// recursion of fields
		Field[] fields = classOfMyObject.getDeclaredFields();
		Arrays.stream(fields).forEach(field -> {
			try {
				field.setAccessible(true);
				Object value = field.get(myObject);
				if (value == null) {
					variables.put(baseName + "." + field.getName(), field.getType().getName());
				} else {
					getAllFieldsAndTypes(baseName + "." + field.getName(), field.get(myObject), variables);
				}
			} catch (Exception t) {
				log.error("Cannot retrieve field from Object", t);
				log.error("field name: {}", field.getName());
				log.error("field class: {}", field.getClass().toString());
			}
		});

	}

	private boolean isPrimitive(Object myObject, Class<? extends Object> classOfMyObject) {
		if (classOfMyObject.isPrimitive() || classOfMyObject.isEnum()) {
			return true;
		}
		if (myObject instanceof String || myObject instanceof Integer || myObject instanceof Double
				|| myObject instanceof Long || myObject instanceof Boolean) {
			return true;
		}
		return myObject instanceof Date || myObject instanceof Instant || myObject instanceof Collection;
	}
}
