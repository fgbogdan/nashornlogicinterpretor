package ro.fagadar.NashornLogicInterpretor.dto;

import lombok.Data;

@Data
public class ObjectA {
	private String oaPropString;
	private int oaPropInt;
	private ObjectB objectB;

}
