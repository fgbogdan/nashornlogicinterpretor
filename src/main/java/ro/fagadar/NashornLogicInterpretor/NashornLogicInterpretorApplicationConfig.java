package ro.fagadar.NashornLogicInterpretor;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import lombok.extern.slf4j.Slf4j;
import ro.fagadar.NashornLogicInterpretor.evaluator.ScriptEngineEvaluator;

@Configuration
@ComponentScan
@SpringBootConfiguration
@Slf4j
public class NashornLogicInterpretorApplicationConfig {

	private ScriptEngine engine;

	@Bean
	@Scope("singleton")
	public ScriptEngineEvaluator getScriptEngine() throws Exception {
		if (engine == null) {
			log.info("Initiate engine");
			ScriptEngineManager factory = new ScriptEngineManager();
			engine = factory.getEngineByName("nashorn");
			if (engine == null) {
				throw new Exception("Unable to create Script engine nashorn");
			}
		}
		return () -> engine;
	}

	@Bean
	@Scope("singleton")
	@ConditionalOnMissingBean
	public ScriptEngineEvaluator getNullScriptEngine() {
		return null;
	}

}
