package ro.fagadar.NashornLogicInterpretor.evaluator;

import javax.script.ScriptEngine;

public interface ScriptEngineEvaluator {
	ScriptEngine getScriptEngine();
}
