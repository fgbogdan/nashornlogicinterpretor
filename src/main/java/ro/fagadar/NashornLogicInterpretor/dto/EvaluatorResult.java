package ro.fagadar.NashornLogicInterpretor.dto;

import lombok.Data;

@Data
public class EvaluatorResult {
    Boolean evaluationResult;

    Boolean evaluationError;

    String evaluationErrorMessage;
}
