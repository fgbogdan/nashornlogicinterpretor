package ro.fagadar.NashornLogicInterpretor.objects;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import ro.fagadar.NashornLogicInterpretor.dto.EvaluatorResult;
import ro.fagadar.NashornLogicInterpretor.dto.ObjectA;
import ro.fagadar.NashornLogicInterpretor.evaluator.NashornEvaluator;

@Component
@Slf4j
public class MainClass {

	@Autowired
	private NashornEvaluator evaluator;

	@Autowired
	public void init() {
		for (Map.Entry<String, Boolean> entry : MainClassConfig.expressions.entrySet()) {
			String expression = entry.getKey();
			Boolean expectedResult = entry.getValue();
			if (expression.isEmpty()) {
				continue;
			}
			log.info("Variables :" + MainClassConfig.variables.toString());
			log.info("Exclusion rule (raw): " + expression);
			EvaluatorResult result = evaluator.evaluateExclusionRule(expression, MainClassConfig.variables);
			log.info("Result: {} - expected result {} ", result.getEvaluationResult(), expectedResult);
		}

		log.info("-----------------------------------");
		ObjectA a = MainClassConfig.getObjectA();

		Map<String, Object> variables1 = evaluator.getAllFieldsAndValues("ObjectA", a);
		log.info(variables1.toString());

		for (Map.Entry<String, Boolean> entry : MainClassConfig.expressionsForObject.entrySet()) {
			String expression = entry.getKey();
			Boolean expectedResult = entry.getValue();
			if (expression.isEmpty()) {
				continue;
			}
			log.info("Variables :" + variables1);
			log.info("Exclusion rule (raw): " + expression);
			EvaluatorResult result = evaluator.evaluateExclusionRule(expression, variables1);
			log.info("Result: {} - expected result {} ", result.getEvaluationResult(), expectedResult);
		}

	}

}
