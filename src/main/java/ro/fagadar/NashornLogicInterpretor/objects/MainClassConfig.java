package ro.fagadar.NashornLogicInterpretor.objects;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ro.fagadar.NashornLogicInterpretor.dto.ObjectA;
import ro.fagadar.NashornLogicInterpretor.dto.ObjectB;

public class MainClassConfig {
	public static int COUNT = 1;

//@formatter:off
	    static Map<String, Object> variables = new HashMap<String, Object>() {
			private static final long serialVersionUID = 1L;

		{
	        put("A", true);
	        put("a", true);
	        put("B", false);
	        put("b", false);
	        put("C", 42.0);
	        put("c", 42.0);
	        put("D", -999.0);
	        put("d", -1999.0);
	        put("E", 42.001);
	        put("e", 142.001);
	        put("F", 42.001);
	        put("f", 42.001);
	        put("G", -1.0);
	        put("g", -1.0);
	        put("ip1", "192.168.2.3");
	        put("ip2", "192.168.1.10");
	        put("ip3", "10.1.1.10");
	    }};
	    
    public static Map<String, Boolean> expressions  = Stream.of(new Object[][] {
		{"1 > 2", false},
		{"1 >= 1.0", true},
		{"true == false", false},
		{"false == false", true},
		{"%A% OR %B%", true},
		{"%B%", false},
		{"%A% == %B%", false},
		{"%c% = %C%", true},
		{"%E% > %D%", true},
		{"%B% OR (%c% = %B% OR (%A% = %A% AND %c% = %C% AND %E% > %D%))", true},
		{"(%A% = %a% OR %B% = %b% OR %C% = %c% AND ((%D% = %d% AND %E% = %e%) OR (%F% = %f% AND %G% = %g%)))", true},
		{"%ip1% == \"192.168.2.3\"", true},
		{" %ip1%.matches(\"192.168.2.*\")", true},
		{" %ip1%.matches(\"192.*.2.*\")", true},
		{" %ip1%.matches(\"*\")", true},
		{" %ip2%.matches(\"192.168.2.*\")", false},
		{" %ip2%.matches(\"192.*.2.*\") AND %c% > 10 ", false},
		{" %ip2%.matches(\"*\")", true},
		{" \"1\"==\"1\" ", true},
		{" %ip1%.matches(\"(r)192\\.168\\.2\\.[01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5]\")", true},
		{" %ip1%.startsWith(\"192.\")", true},
		{" %ip1%.startsWith(\"xxx.\")", false},
		{" %ip1%.endsWith(\"2.3\")", true},
		{" %ip1%.endsWith(\"xxx\")", false},
		{" %ip1%.contains(\".2.\")", true},
		{" %ip1%.contains(\"xxx\")", false},
		{" ['a', 'b', 'c'].indexOf('b') > -1 ", true},
		{" ['a', 'b', 'c'].indexOf('x') > -1 ", false},
		{" '/my/folder/MyFile.txt'.matches('.*My.*') && 'FILE' == 'FILE' || 'SID_SOMETHING' == 'SID_SOMETHING' && ('1.2.66.100' == '192.168.2.3' || '1.2.66.100' == '1.2.66.100') ", true},
		{" \"/my/folder/MyFile.txt\".matches('.*My.*') && false || \"SID_SOMETHING\" == 'SID_SOMETHING' && (\"1.2.66.100\" == '192.168.2.3' || \"1.2.66.100\" == '1.2.66.100')", true}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (Boolean) data[1]));
    
    
    public static ObjectA getObjectA() {
    	ObjectB b = new ObjectB();
    	b.setObPropBoolean(true);
    	b.setObPropInt(2);
    	b.setObPropString("ObjectB");
    	ObjectA a = new ObjectA();
    	a.setOaPropInt(3);
    	a.setOaPropString("ObjectA");
    	a.setObjectB(b);
    	return a;
    }
    
    public static Map<String, Boolean> expressionsForObject  = Stream.of(new Object[][] {
    	{"%ObjectA.objectB.obPropString%==\"ObjectB\"", true},
    	{"%ObjectA.objectB.obPropString%!=\"ObjectB\"", false},
    	{"%ObjectA.objectB.obPropString%.contains(\"j\")", true},
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (Boolean) data[1]));    
	
}
