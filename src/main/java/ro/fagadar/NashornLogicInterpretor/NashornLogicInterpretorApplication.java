package ro.fagadar.NashornLogicInterpretor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import ro.fagadar.NashornLogicInterpretor.objects.MainClass;

@SpringBootApplication
public class NashornLogicInterpretorApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext configurableApplicationContext = SpringApplication
				.run(NashornLogicInterpretorApplication.class, args);
		configurableApplicationContext.getBean(MainClass.class).init();
	}
}
