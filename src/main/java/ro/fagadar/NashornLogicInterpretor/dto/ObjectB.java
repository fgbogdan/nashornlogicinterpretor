package ro.fagadar.NashornLogicInterpretor.dto;

import lombok.Data;

@Data
public class ObjectB {

	private String obPropString;
	private int obPropInt;
	private boolean obPropBoolean;

}
